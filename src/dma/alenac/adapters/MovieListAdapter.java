package dma.alenac.adapters;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import dma.alenac.models.Movie;
import dma.alenac.moviebrowserv2.R;

public class MovieListAdapter extends ArrayAdapter<Movie> {
	private LayoutInflater layout_inflater;
	private ImageLoader image_loader = null;

	public MovieListAdapter(Context context, int item_layout_id,
			int default_text_id, List<Movie> movies, ImageLoader image_loader) {
		super(context, item_layout_id, default_text_id, movies);
		layout_inflater = LayoutInflater.from(context);
		this.image_loader = image_loader;
	}

	/**
	 * Fills the list view with data
	 */
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		if (convertView == null) {
			convertView = layout_inflater.inflate(R.layout.movie_list_item,
					null);

			holder = new ViewHolder();

			// use the Volley library's NetworkImageView
			holder.thumbnail = (NetworkImageView) convertView
					.findViewById(R.id.movie_list_thumbnail);
			holder.title = (TextView) convertView
					.findViewById(R.id.movie_list_title);
			holder.mpaaRating = (TextView) convertView
					.findViewById(R.id.movie_list_mpaaRating);
			holder.rating = (RatingBar) convertView
					.findViewById(R.id.movie_list_rating);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Movie movie = this.getItem(position);

		// Volley method to load network image into the view
		holder.thumbnail.setImageUrl(movie.thumbnailUrl, image_loader);

		holder.title.setText(movie.title);
		holder.mpaaRating.setText(movie.mpaaRating);
		if (movie.mpaaRating.equals("MPAA rating unavailable")) {
			holder.mpaaRating.setTypeface(null, Typeface.ITALIC);
		} else {
			holder.mpaaRating.setTypeface(null, Typeface.NORMAL);
		}
		holder.rating.setRating(movie.rating);

		return convertView;
	}

	static class ViewHolder {
		NetworkImageView thumbnail;
		TextView title;
		TextView mpaaRating;
		RatingBar rating;
	}
}
