package dma.alenac.interfaces;

import dma.alenac.models.Movie;

/**
 * Listener for when a movie is selected in the list view
 */
public interface MovieSelectionListener {
	public void onMovieSelected(Movie movie);
}
