package dma.alenac.interfaces;

import org.json.JSONObject;

/**
 * Listener for when the navigation dropdown list changes
 */
public interface IMDBResponseListener {
	public void onIMDBMovieListResponse(JSONObject json_object);
}
