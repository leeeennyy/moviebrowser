package dma.alenac.imdbclient;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dma.alenac.models.Movie;
import android.graphics.BitmapFactory;
import android.util.Log;

public class JSONParser {
	private static final String TAG = "JSONParser";

	/**
	 * Parses the JSONObject to retrieve values to create a new movie
	 * 
	 * @param json_movie
	 *            The JSON for a movie
	 * @return A new movie with the metadata retrieved from the JSONObject
	 */
	public static Movie parseMovieJSON(JSONObject json_movie) {
		try {
			Movie movie = new Movie();
			movie.title = json_movie.optString("title");
			movie.mpaaRating = json_movie.optString("rated");
			movie.mpaaRating = movie.mpaaRating.equals("") ? "MPAA rating unavailable"
					: movie.mpaaRating;
			movie.imdbId = json_movie.optString("idIMDB");
			movie.plot = json_movie.optString("plot");
			movie.returnUrl = json_movie.optString("urlIMDB");
			movie.thumbnailUrl = json_movie.optString("urlPoster");
			movie.rating = json_movie.optString("rating").equals("") ? 0
					: Float.parseFloat(json_movie.optString("rating")) / 2;
			try {
				InputStream in = new URL(movie.thumbnailUrl).openStream();
				movie.thumbnail = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.d("JSONParser", "Exception loading bitmap from URL");
			}

			return movie;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Retrieves all movies from the API by parsing the JSON
	 * 
	 * @param json_movie_list
	 *            The JSONObject which contains a JSONArray of all movies
	 * @return A list of movies
	 */
	public static ArrayList<Movie> parseMovieListJSON(JSONObject json_movie_list) {
		ArrayList<Movie> movies = new ArrayList<Movie>();

		try {
			JSONArray movie_list = json_movie_list.getJSONArray("movies");

			for (int i = 0; i < movie_list.length(); i++) {
				JSONObject movie = movie_list.getJSONObject(i);
				Movie m = parseMovieJSON(movie);
				if (m != null)
					movies.add(m);
			}
		} catch (JSONException e) {
			Log.d(TAG, "JSONException");
			e.printStackTrace();
			return movies;
		}
		return movies;
	}
}
