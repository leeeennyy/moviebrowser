package dma.alenac.imdbclient;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import dma.alenac.interfaces.IMDBResponseListener;
import dma.alenac.utils.BitmapCache;

public class IMDBClientFragment extends Fragment {
	private static final String TAG = "IMDBClientFragment";

	public static final String IN_THEATERS_URL = "http://www.myapifilms.com/imdb/inTheaters";
	public static final String COMING_SOON_URL = "http://www.myapifilms.com/imdb/comingSoon";

	// Volley queue, cache, image loader
	private RequestQueue request_queue = null;
	private ImageLoader image_loader = null;
	private BitmapCache bitmap_cache = null;

	private IMDBResponseListener imdb_response_listener;

	public IMDBClientFragment() {
	}

	/**
	 * Ensures that the hosting activity implements the response listener
	 * interface
	 */
	@Override
	public void onAttach(Activity activity) {
		Log.d(TAG, "onAttach()");
		super.onAttach(activity);
		try {
			imdb_response_listener = (IMDBResponseListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement IMDBResponseListener");
		}
	}

	/**
	 * This method is called only once when the Fragment is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "onCreate(Bundle)");
		super.onCreate(savedInstanceState);

		// initialise the Volley queue and image loader
		request_queue = Volley.newRequestQueue(getActivity()
				.getApplicationContext());
		bitmap_cache = new BitmapCache();
		image_loader = new ImageLoader(request_queue, bitmap_cache);

		Log.i(TAG, "onCreate(Bundle) : " + getActivity().hashCode());
		Log.i(TAG, "onCreate(Bundle) : "
				+ getActivity().getApplicationContext().hashCode());

		// keep state across config changes (we don't lose the queue and loader)
		setRetainInstance(true);
	}

	public ImageLoader getImageLoader() {
		return image_loader;
	}

	public BitmapCache getBitmapCache() {
		return bitmap_cache;
	}

	public void cancelAllRequests() {
		request_queue.cancelAll(this);
	}

	@Override
	public void onStop() {
		Log.d(TAG, "onStop");
		super.onStop();
		cancelAllRequests();
	}

	/**
	 * Issue requests to MyAPIFilms and return responses to the registered
	 * listener
	 * 
	 * @param The
	 *            index of the item selected in the navigation dropwdown list on
	 *            action bar
	 */
	public void getMovieList(int index) {
		String request_url = "";
		if (index == 0)
			request_url = IN_THEATERS_URL;
		if (index == 1)
			request_url = COMING_SOON_URL;

		JsonArrayRequest request = new JsonArrayRequest(Method.GET,
				request_url, null, new Listener<JSONArray>() {
			public void onResponse(JSONArray json_array) {
				Log.d(TAG, "onResponse");
				try {
					imdb_response_listener
					.onIMDBMovieListResponse(json_array
							.getJSONObject(1));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}, new ErrorListener() {
			public void onErrorResponse(VolleyError error) {
				Log.d(TAG,
						"getMovieList : onErrorResponse : "
								+ error.getMessage());
				error.printStackTrace();
				imdb_response_listener.onIMDBMovieListResponse(null);
			}
		});

		request_queue.add(request);
	}
}
