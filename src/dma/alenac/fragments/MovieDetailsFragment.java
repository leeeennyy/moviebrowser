package dma.alenac.fragments;

import dma.alenac.models.Movie;
import dma.alenac.moviebrowserv2.R;
import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

public class MovieDetailsFragment extends Fragment {
	private static final String TAG = "MovieDetailsFragment";

	public MovieDetailsFragment() {
	}

	/**
	 * Initializes the fragment
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView()");
		View details_view = inflater.inflate(R.layout.movie_details_fragment,
				container, false);

		return details_view;
	}

	/**
	 * Update the UI with the new movie details
	 * 
	 * @param movie
	 *            The movie that the user has selected in the list view
	 */
	public void update(Movie movie) {
		Log.d(TAG, "setContent()");

		TextView title_view = (TextView) getView().findViewById(
				R.id.movie_details_title);
		TextView plot_view = (TextView) getView().findViewById(
				R.id.movie_details_plot);
		RatingBar rating_view = (RatingBar) getView().findViewById(
				R.id.movie_details_rating);
		TextView mpaa_view = (TextView) getView().findViewById(
				R.id.movie_details_mpaaRating);

		title_view.setText(movie.title);
		plot_view.setText(movie.plot);
		mpaa_view.setText(movie.mpaaRating);
		if (movie.mpaaRating.equals("MPAA rating unavailable")) {
			mpaa_view.setTypeface(null, Typeface.ITALIC);
		} else {
			mpaa_view.setTypeface(null, Typeface.NORMAL);
		}
		rating_view.setVisibility(View.VISIBLE);
		rating_view.setRating(movie.rating);
	}

	/**
	 * Clears the details fragment
	 */
	public void clear() {
		Log.d(TAG, "clear()");

		TextView title_view = (TextView) getView().findViewById(
				R.id.movie_details_title);
		TextView plot_view = (TextView) getView().findViewById(
				R.id.movie_details_plot);
		RatingBar rating_view = (RatingBar) getView().findViewById(
				R.id.movie_details_rating);
		TextView mpaa_view = (TextView) getView().findViewById(
				R.id.movie_details_mpaaRating);

		title_view.setText("");
		plot_view.setText("");
		mpaa_view.setText("");
		rating_view.setRating(0f);
		rating_view.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
		super.onCreate(savedInstanceState);
	}
}
