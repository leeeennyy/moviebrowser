package dma.alenac.fragments;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import dma.alenac.activities.MovieBrowserActivity;
import dma.alenac.adapters.MovieListAdapter;
import dma.alenac.interfaces.MovieSelectionListener;
import dma.alenac.models.Movie;
import dma.alenac.moviebrowserv2.R;

public class MovieListFragment extends ListFragment {
	private static final String TAG = "MovieListFragment";
	private static final int NO_SELECTION = -1;

	private ArrayList<Movie> movies = new ArrayList<Movie>();
	private MovieSelectionListener movie_selection_listener;
	private MovieListAdapter movie_list_adapter;

	private int selected_item_position = NO_SELECTION;

	/*
	 * requires empty constructor
	 */
	public MovieListFragment() {
	}

	/**
	 * This fragment has now been added to the activity, which we use as the
	 * listener for movie list selections
	 */
	@Override
	public void onAttach(Activity activity) {
		Log.d(TAG, "onAttach()");
		super.onAttach(activity);
		try {
			movie_selection_listener = (MovieSelectionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement MovieSelectionListener");
		}
	}

	/**
	 * Set up the data/list view adapter
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			Log.d(TAG,
					"onActivityCreated() : getting selected_item_position from savedInstanceState");
			selected_item_position = savedInstanceState
					.getInt("selected_item_position");
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d(TAG, "onActivityCreated()");
		super.onActivityCreated(savedInstanceState);

		movie_list_adapter = new MovieListAdapter(getActivity(),
				R.layout.movie_list_item, R.id.movie_list_title, movies,
				((MovieBrowserActivity) getActivity()).getImageLoader());

		getListView().setAdapter(movie_list_adapter);

	}

	/**
	 * The fragment is visible and 'alive'. Now we can do UI operations.
	 */
	public void onResume() {
		Log.d(TAG, "onResume()");
		super.onResume();

		getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		setIsLoading(false);

		if (movie_list_adapter.getCount() > 0
				&& selected_item_position != NO_SELECTION) {
			getListView().setItemChecked(selected_item_position, true);
			getListView().setSelection(selected_item_position);
			getListView().smoothScrollToPositionFromTop(selected_item_position,
					200, 0);
			movie_selection_listener.onMovieSelected(movie_list_adapter
					.getItem(selected_item_position));
		}
	}

	/**
	 * Shows/Hides the loading spinner
	 * 
	 * @param is_loading
	 *            whether it is retrieving from the network
	 */
	public void setIsLoading(boolean is_loading) {
		setListShown(!is_loading);
	}

	/**
	 * Lets the listener know that a movie has been selected to display it
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Log.d(TAG, "onListItemClick() : " + position);
		selected_item_position = position;

		getListView().setItemChecked(selected_item_position, true);

		movie_selection_listener.onMovieSelected(movie_list_adapter
				.getItem(selected_item_position));
	}

	/**
	 * Updates the list of movies
	 * 
	 * @param retrieved_movies
	 *            The movies retrieved over the network
	 * @param movie_changed
	 *            If the navigation bar dropdown in the main activity is changed
	 */
	public void update(ArrayList<Movie> retrieved_movies,
			boolean movie_type_changed) {
		Log.d(TAG, "update() : changed = " + movie_type_changed);

		setIsLoading(false);

		movies.clear();
		movies.addAll(retrieved_movies);
		movie_list_adapter.notifyDataSetChanged();

		if (movie_type_changed) {
			getListView().setItemChecked(selected_item_position, false);
			selected_item_position = NO_SELECTION;
			getListView().setSelection(0);
		} else if (movie_list_adapter.getCount() > 0
				&& selected_item_position != NO_SELECTION) {
			getListView().setItemChecked(selected_item_position, true);
			getListView().setSelection(selected_item_position);
			getListView().smoothScrollToPositionFromTop(selected_item_position,
					200, 0);
			movie_selection_listener.onMovieSelected(movie_list_adapter
					.getItem(selected_item_position));
		}
	}

	/**
	 * Return the currently selected movie
	 */
	public Movie getSelectedMovie() {
		if (movie_list_adapter.getCount() > 0
				&& selected_item_position != NO_SELECTION) {
			return movie_list_adapter.getItem(selected_item_position);
		} else {
			return null;
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putInt("selected_item_position", selected_item_position);
		Log.d(TAG, "onSaveInstanceState() : selected_item_position = "
				+ selected_item_position);
		super.onSaveInstanceState(outState);
	}

	public void sortByTitleAsc() {
		Collections.sort(movies, Movie.COMPARE_BY_TITLE_ASC);
		movie_list_adapter.notifyDataSetChanged();
		getListView().setSelection(0);
	}

	public void sortByRatingAsc() {
		Collections.sort(movies, Movie.COMPARE_BY_RATING_ASC);
		movie_list_adapter.notifyDataSetChanged();
		getListView().setSelection(0);
	}

	public void sortByRatingDesc() {
		Collections.sort(movies, Movie.COMPARE_BY_RATING_DESC);
		movie_list_adapter.notifyDataSetChanged();
		getListView().setSelection(0);
	}

	@Override
	public void onStart() {
		Log.d(TAG, "onStart()");
		super.onStart();
	}

	@Override
	public void onPause() {
		Log.d(TAG, "onPause()");
		super.onPause();
	}

	@Override
	public void onStop() {
		Log.d(TAG, "onStop()");
		super.onStop();
	}

	@Override
	public void onDestroyView() {
		Log.d(TAG, "onDestroyView()");
		super.onDestroyView();
	}

	@Override
	public void onDetach() {
		Log.d(TAG, "onDetach()");
		super.onDetach();
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy()");
		super.onDestroy();
	}
}
