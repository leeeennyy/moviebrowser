package dma.alenac.models;

import java.util.Comparator;
import java.util.Date;

import android.graphics.Bitmap;

/**
 * A movie with metadata
 */
public class Movie {
	public String title;
	public String imdbId;
	public String plot;
	public String returnUrl;
	public String thumbnailUrl;
	public String mpaaRating;
	public String releaseDate;
	public float rating;
	public Bitmap thumbnail;

	public Movie() {
	}

	public Movie(String title, String imdbId, String plot, String returnUrl,
			String thumbnailUrl, String mpaaRating, float rating) {
		this.title = title;
		this.imdbId = imdbId;
		this.plot = plot;
		this.returnUrl = returnUrl;
		this.thumbnailUrl = thumbnailUrl;
		this.mpaaRating = mpaaRating;
		this.rating = rating;
	}

	public static Comparator<Movie> COMPARE_BY_TITLE_ASC = new Comparator<Movie>() {
		public int compare(Movie one, Movie other) {
			return one.title.compareTo(other.title);
		}
	};

	public static Comparator<Movie> COMPARE_BY_RATING_ASC = new Comparator<Movie>() {
		public int compare(Movie one, Movie other) {
			if (one.rating < other.rating)
				return -1;
			else if (one.rating > other.rating)
				return 1;
			else
				return 0;
		}
	};

	public static Comparator<Movie> COMPARE_BY_RATING_DESC = new Comparator<Movie>() {
		public int compare(Movie one, Movie other) {
			if (one.rating < other.rating)
				return 1;
			else if (one.rating > other.rating)
				return -1;
			else
				return 0;
		}
	};
}
