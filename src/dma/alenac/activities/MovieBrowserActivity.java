package dma.alenac.activities;

import org.json.JSONObject;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SlidingPaneLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;

import dma.alenac.fragments.MovieDetailsFragment;
import dma.alenac.fragments.MovieListFragment;
import dma.alenac.imdbclient.IMDBClientFragment;
import dma.alenac.imdbclient.JSONParser;
import dma.alenac.interfaces.IMDBResponseListener;
import dma.alenac.interfaces.MovieSelectionListener;
import dma.alenac.models.Movie;
import dma.alenac.moviebrowserv2.R;

@SuppressWarnings("deprecation")
public class MovieBrowserActivity extends Activity implements
		IMDBResponseListener, MovieSelectionListener, OnNavigationListener {

	private static final String TAG = "MovieBrowserActivity";

	private static final String IMDB_CLIENT_FRAGMENT_TAG = "IMDBClientFragment";
	private static final String MOVIE_LIST_FRAGMENT_TAG = "MovieListFragment";
	private static final String MOVIE_DETAILS_FRAGMENT_TAG = "MovieDetailsFragment";
	private static final String SELECTED_MOVIE_TYPE_IDX = "SELECTED_MOVIE_IDX";
	private static final int NO_SELECTION = -1;

	// stores position of currently selected movie from menu so that we can
	// store/restore it on config change
	private int selected_movie_type_position = NO_SELECTION;
	private boolean movie_type_changed = false;

	// boolean variable to check/show the up navigation properly between
	// orientation changes
	private boolean movie_selected_landscape = false;
	private boolean show_details = false;
	// private boolean movie_selected = false;

	// adapter for menu in action bar
	private ArrayAdapter<String> action_bar_nav_adapter;

	// fragment manager and dynamic fragments
	private FragmentManager fragment_manager;
	private IMDBClientFragment imdb_client_fragment;

	private MenuItem menu_sort;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.movie_browser_activity);

		SlidingPaneLayout sliding_layout = (SlidingPaneLayout) findViewById(R.id.sliding_layout);
		sliding_layout.setSliderFadeColor(getResources().getColor(
				android.R.color.transparent));
		sliding_layout.setPanelSlideListener(new SliderListener());
		sliding_layout.openPane();

		fragment_manager = getFragmentManager();

		addNonUIFragments();

		if (savedInstanceState != null) {
			selected_movie_type_position = savedInstanceState
					.getInt(SELECTED_MOVIE_TYPE_IDX);
			Log.d(TAG, "onCreate() : retrieved selected movie position = "
					+ selected_movie_type_position);
		}

		setUpActionBar();
	}

	/**
	 * Configures the ActionBar appearance and behavior. Adds a predefined list
	 * of items to the ActionBar navigation drop down list and register the
	 * activity to handle navigation selections.
	 */
	private void setUpActionBar() {
		Log.d(TAG, "setUpActionBar()");

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		String[] movie_type_list = getResources().getStringArray(
				R.array.nav_list);

		action_bar_nav_adapter = new ArrayAdapter<String>(
				actionBar.getThemedContext(),
				android.R.layout.simple_list_item_1, android.R.id.text1);
		action_bar_nav_adapter.addAll(movie_type_list);

		actionBar.setListNavigationCallbacks(action_bar_nav_adapter, this);
	}

	/**
	 * Gets reference to the IMDB Client fragment. If we haven't already
	 * instantiated it (this is a 'clean' start of the activity) then create an
	 * instance and add it to the activity (but not in the UI)
	 */
	private void addNonUIFragments() {
		Log.d(TAG, "addNonUIFragments()");

		imdb_client_fragment = (IMDBClientFragment) fragment_manager
				.findFragmentByTag(IMDB_CLIENT_FRAGMENT_TAG);

		FragmentTransaction ft = fragment_manager.beginTransaction();

		if (imdb_client_fragment == null) {
			imdb_client_fragment = new IMDBClientFragment();
			ft.add(imdb_client_fragment, IMDB_CLIENT_FRAGMENT_TAG);
		}

		ft.commit();
		fragment_manager.executePendingTransactions();
	}

	@Override
	public void onIMDBMovieListResponse(JSONObject json_object) {
		Log.d(TAG, "onIMDBMovieListResponse()");

		MovieListFragment movie_list_fragment = (MovieListFragment) fragment_manager
				.findFragmentByTag(MOVIE_LIST_FRAGMENT_TAG);
		if (json_object == null) {
			Toast.makeText(
					this,
					"Unable to connect to server.\nPlease check your network or try again.",
					Toast.LENGTH_LONG).show();
			movie_list_fragment.setIsLoading(false);
		} else {
			if (movie_list_fragment != null) {
				movie_list_fragment.update(
						JSONParser.parseMovieListJSON(json_object),
						movie_type_changed);
			}
		}

	}

	/**
	 * Retrieves the movie list if the navigation item changes over the network
	 * and changes what the user sees appropriately.
	 */
	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		Log.d(TAG, "onNavigationItemSelected : " + itemPosition);

		movie_type_changed = selected_movie_type_position != itemPosition;
		selected_movie_type_position = itemPosition;
		imdb_client_fragment.getMovieList(itemPosition);

		MovieListFragment movie_list_fragment = (MovieListFragment) fragment_manager
				.findFragmentByTag(MOVIE_LIST_FRAGMENT_TAG);
		if (movie_list_fragment != null) {
			movie_list_fragment.setIsLoading(true);
		}

		if (movie_type_changed) {
			MovieDetailsFragment movie_details_fragment = (MovieDetailsFragment) fragment_manager
					.findFragmentByTag(MOVIE_DETAILS_FRAGMENT_TAG);
			if (movie_details_fragment != null) {
				movie_details_fragment.clear();
			}
			SlidingPaneLayout sliding_layout = (SlidingPaneLayout) findViewById(R.id.sliding_layout);
			sliding_layout.openPane();
		}
		return true;
	}

	/**
	 * ActionBar items selection handler
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		MovieListFragment movie_list_fragment = (MovieListFragment) fragment_manager
				.findFragmentByTag(MOVIE_LIST_FRAGMENT_TAG);

		switch (item.getItemId()) {
		case android.R.id.home:
			SlidingPaneLayout sliding_layout = (SlidingPaneLayout) findViewById(R.id.sliding_layout);
			if (sliding_layout.isOpen() == false) {
				sliding_layout.openPane();
			}
			return true;
		case R.id.action_sort_rating_asc:
			if (movie_list_fragment != null) {
				movie_list_fragment.sortByRatingAsc();
			}
			return true;
		case R.id.action_sort_rating_desc:
			if (movie_list_fragment != null) {
				movie_list_fragment.sortByRatingDesc();
			}
			return true;
		case R.id.action_sort_title_asc:
			if (movie_list_fragment != null) {
				movie_list_fragment.sortByTitleAsc();
			}
			return true;
		case R.id.action_webpage:
			Movie m = movie_list_fragment.getSelectedMovie();
			if (m != null) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW,
						Uri.parse(m.returnUrl));
				startActivity(browserIntent);
			} else {
				Toast.makeText(this, "Please select a movie first.",
						Toast.LENGTH_SHORT).show();
			}
			return true;
		case R.id.action_legal_notices:
			Toast.makeText(this, "Legal Notices", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.action_settings:
			Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Invoked from movie list fragment when item is clicked
	 */
	@Override
	public void onMovieSelected(Movie movie) {
		Log.d(TAG, "onMovieSelected() : " + movie.title);
		// movie_selected = true;

		Configuration c = getResources().getConfiguration();
		if (c.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			movie_selected_landscape = true;
		} else if (c.orientation == Configuration.ORIENTATION_PORTRAIT) {
			SlidingPaneLayout sliding_layout = (SlidingPaneLayout) findViewById(R.id.sliding_layout);
			sliding_layout.closePane();
			movie_selected_landscape = false;
		}

		MovieDetailsFragment movie_details_fragment = (MovieDetailsFragment) fragment_manager
				.findFragmentByTag(MOVIE_DETAILS_FRAGMENT_TAG);
		if (movie_details_fragment != null) {
			movie_details_fragment.update(movie);
		}
	}

	public ImageLoader getImageLoader() {
		return imdb_client_fragment.getImageLoader();
	}

	@Override
	protected void onStop() {
		Log.d(TAG, "onStop");
		super.onStop();
		// make sure all pending network requests are cancelled when this
		// activity stops
		if (imdb_client_fragment != null) {
			imdb_client_fragment.cancelAllRequests();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		Log.d(TAG, "onSaveInstanceState");
		int selected_movie_idx = getActionBar().getSelectedNavigationIndex();
		outState.putInt(SELECTED_MOVIE_TYPE_IDX, selected_movie_idx);
		super.onSaveInstanceState(outState);

		Log.d(TAG, "onSaveInstanceState() : stored selected movie position = "
				+ selected_movie_idx);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.d(TAG, "onCreateOptionsMenu()");
		getMenuInflater().inflate(R.menu.activity_movie_browser, menu);

		menu_sort = (MenuItem) menu.findItem(R.id.action_sort);
		return true;
	}

	@Override
	public void onBackPressed() {
		SlidingPaneLayout sliding_layout = (SlidingPaneLayout) findViewById(R.id.sliding_layout);
		if (sliding_layout.isOpen()) {
			super.onBackPressed();
		} else {
			sliding_layout.openPane();
		}
	}

	/**
	 * Shows/Hides the 'up' button in actionbar depending on how the device was
	 * oriented
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		int orientation = newConfig.orientation;

		SlidingPaneLayout sliding_layout = (SlidingPaneLayout) findViewById(R.id.sliding_layout);

		switch (orientation) {
		case Configuration.ORIENTATION_LANDSCAPE:
			getActionBar().setHomeButtonEnabled(false);
			getActionBar().setDisplayHomeAsUpEnabled(false);
			menu_sort.setVisible(true);
			break;
		case Configuration.ORIENTATION_PORTRAIT:
			if (movie_selected_landscape) {
				getActionBar().setHomeButtonEnabled(false);
				getActionBar().setDisplayHomeAsUpEnabled(false);
			} else if (show_details) {
				getActionBar().setHomeButtonEnabled(true);
				getActionBar().setDisplayHomeAsUpEnabled(true);
				menu_sort.setVisible(false);
			}
			break;
		}
	}

	/**
	 * This panel slide listener updates the action bar accordingly for each
	 * panel state.
	 */
	private class SliderListener extends
			SlidingPaneLayout.SimplePanelSlideListener {
		@Override
		public void onPanelOpened(View panel) {
			getActionBar().setHomeButtonEnabled(false);
			getActionBar().setDisplayHomeAsUpEnabled(false);
			menu_sort.setVisible(true);
			show_details = false;
		}

		@Override
		public void onPanelClosed(View panel) {
			getActionBar().setHomeButtonEnabled(true);
			getActionBar().setDisplayHomeAsUpEnabled(true);
			menu_sort.setVisible(false);
			show_details = true;
		}
	}
}
